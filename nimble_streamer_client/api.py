import base64
import hashlib
import json
import random
import asyncio

import aiohttp

from nimble_streamer_client.exceptions import InvalidAPIResponseError

__all__ = [
    "NimbleAPIClient"
]

def __get_auth_params(token: str) -> dict:
    salt = random.randint(0, 100000)

    md5_hash = hashlib.md5(f"{salt}/{token}".encode()).digest()

    base64_hash = base64.b64encode(md5_hash).decode()

    return {
        'salt': salt,
        'hash': base64_hash
    }

class NimbleAPIClient():
    def __init__(self, api_url: str, token: str = ""):
        self._api_url = api_url
        self._auth_params = None

        if token != "":
            self._auth_params = __get_auth_params(token)

        self._client_session = aiohttp.ClientSession(timeout=aiohttp.ClientTimeout(total=5))

    async def __aenter__(self):
        return self

    async def __aexit__(self, exc_t, exc_v, exc_tb):
        await self._client_session.close()

    async def __request(self, endpoint_location: str, method: str):
        endpoint_url = f'{self._api_url}{endpoint_location}'

        if self._auth_params is not None:
            endpoint_url += f'?{"&".join("=".join(_) for _ in self._auth_params.items())}'

        try:
            async with self._client_session.request(method=method, url=endpoint_url) as response:
                try:
                    return json.loads(await response.text())
                except json.JSONDecodeError as err:
                    raise InvalidAPIResponseError(response.text()) from err
        except aiohttp.client_exceptions.ClientConnectorError as exc:
            print(f"Request failed: {str(exc)}")
        except asyncio.exceptions.TimeoutError as exc:
            print(f"Request timeout: {str(exc)}")

    async def reload_config(self):
        return await self.__request('/manage/reload_config', 'POST')

    async def server_status(self):
        return await self.__request('/manage/server_status', 'GET')
