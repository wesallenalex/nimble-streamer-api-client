__all__ = [
    "InvalidAPIResponseError",
    "InvalidURLError"
]

class InvalidAPIResponseError(RuntimeError):
    pass

class InvalidURLError(RuntimeError):
    pass
