from setuptools import setup, find_packages

setup(
    name='nimble-streamer-api-client',
    version="0.1.0",
    description='Nimble Streamer API client for python',
    url='https://gitlab.com/wesallenalex/nimble-streamer-api-client',
    author='Wexter',
    author_email='wesallenalex@gmail.com',
    license='',
    packages=find_packages(),
    install_requires=[
        "asyncio",
        "aiohttp",
        "aiodns",
    ],
    classifiers=[
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
    ],
)
